{ lib, pkgs, inputs, ... }:

{
  environment = lib.mkMerge [{
    systemPackages =
    #   with inputs.nix-gaming.packages.${pkgs.hostPlatform.system}; [
    #   # wine-ge - this is just ge-8.26 (i.e., dont bother)
    #   wine-tkg
    # ] ++ #(with inputs.emacs-overlay.packages.${pkgs.hostPlatform.system}; [
    #  emacs30#-unstable
    #]) ++
    (with pkgs; [
      #
      # APPLICATIONS
      #
      brave
      libreoffice
      obs-studio
      unityhub
      vivaldi
      webcord
      #
      # GAMING
      #
      lutris
      heroic
      mangohud
      protontricks
      protonup-qt
      steam-run
      wineWowPackages.staging
      winetricks
      #
      # PLAYERS
      #
      mpv
      vlc
      #
      # EDITORS
      #
      (unstable.emacs30.override {
        withGTK3 = false;
        withPgtk = false;
        toolkit = "lucid";
        noGui = false;
      })
      ## for emacs
      gsettings-desktop-schemas
      helix
      vim
      #
      # BUILD BASE
      #
      cmake
      stdenv.cc
      #
      # PROGRAM LANGUAGES
      #
      python3
      lua
      nodejs_22
      #
      # PROGRAM LANGUAGE SUPPORT
      #
      emacs-lsp-booster
      fzf
      jq
      lua52Packages.lua-lsp
      marksman
      pyright
      tree-sitter
      typescript-language-server
      #
      # NIX CONFIG/DEV
      #
      nil
      # nixd
      nixfmt
      #
      # CODECS AUDIO / VIDEO
      #
      alsa-lib
      alsa-tools
      alsa-utils
      ffmpeg-full
      gst_all_1.gstreamer
      gst_all_1.gst-plugins-base
      gst_all_1.gst-plugins-good
      gst_all_1.gst-plugins-bad
      gst_all_1.gst-plugins-ugly
      gst_all_1.gst-libav
      gst_all_1.gst-vaapi
      #
      # GRAPHICS
      #
      vkbasalt
      #
      # DESKTOP
      #
      wayland
      weston
      wlroots
      #
      # GTK
      #
      gtk2
      gtk3
      gtk4
      #
      # LIBRARIES
      #
      libdrm
      libglvnd
      libpulseaudio
      libgdiplus
      libGL
      libgdiplus # for win compat for games
      libinput
      libnotify
      libsecret
      libtool
      libva
      libvpx
      libvterm
      #
      # QT STUFF
      #
      qt5.qtbase
      qt6.qtbase
      #
      # TERMINALS
      #
      alacritty
      kitty
      wezterm
      #
      # TEXT-TO-SPEECH
      #
      piper-tts
      #
      # TOOLS
      #
      glxinfo
      gparted
      networkmanagerapplet
      pavucontrol
      vulkan-tools
      xarchiver
      #
      # SHELLS
      #
      ## fish enabled in programs
      nushell
      zsh
      #
      # THEMES
      #
      adapta-kde-theme
      adapta-gtk-theme
      candy-icons
      numix-gtk-theme
      numix-icon-theme
      paper-gtk-theme
      papirus-icon-theme
      #
      # UTILS
      #
      bat
      cachix
      dolphin
      eza
      fd
      git-extras
      gwenview
      hunspell
      hunspellDicts.en_US
      kgpg
      libva-utils
      p7zip
      redshift
      ripgrep
      sd
      spectacle # for screenshots
      starship
      tealdeer
      unzip
      vdpauinfo
      wget
      zellij
      zip
      zoxide
    ]);
  }];
  
  fonts = lib.mkMerge [{
    enableDefaultPackages = true;
    packages = with pkgs; [
      fira-code
      fira-code-symbols
      fira-mono
      hack-font
      monaspace
      nerdfonts
      noto-fonts
    ];
  }];

  programs = lib.mkMerge [{
    # ensure gtk apps in kde wayland are styled properly
    dconf.enable = true;

    firefox = {
      enable = true;
      # package = pkgs.latest.firefox-bin;
    };

    fish = {
      enable = true;
      useBabelfish = true;
      vendor.completions.enable = true;
    };
    
    gamemode = {
      enable = true;
      enableRenice = true;
    };

    gamescope = {
      enable = true;
      # disable this due to conflicts with steam
      capSysNice = false;
    };
    
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };

    git = {
      enable = true;
      config = {
        init = {
          defaultBranch = "main";
        };
        user = {
          name = "Erika Jonell";
          email = "erika.jonel@gmail.com";
        };
        core = {
          editor = "emacs -nw";
        };
        merge = {
          conflictstyle = "diff3";
        };
        pull = {
          rebase = "false";
        };
        # credential = {
        #   helper = "${
        #   pkgs.git.override { withLibsecret = true; }
        #   }/bin/git-credential-libsecret";
        # };
      };
    };

    # nix-ld = {
    #   enable = true;
    #   package = pkgs.nix-ld-rs;
    #   libraries = with pkgs; [
    #     # std env
    #     stdenv.cc.cc
    #     # general
    #     glib
    #     icu
    #     libinput
    #     libnotify
    #     libsecret
    #     libvterm
    #     util-linux
    #     zlib
    #     zstd
    #     # good dev stuff too
    #     alsa-lib
    #     cairo
    #     freetype
    #     fontconfig
    #     libdrm
    #     libglvnd
    #     libpulseaudio
    #     libgdiplus
    #     libGL
    #     libxkbcommon
    #     gdk-pixbuf
    #     pipewire
    #     vulkan-loader
    #     xorg.libX11
    #     xorg.libXScrnSaver
    #     xorg.libXcomposite
    #     xorg.libXcursor
    #     xorg.libXdamage
    #     xorg.libXext
    #     xorg.libXfixes
    #     xorg.libXi
    #     xorg.libXrandr
    #     xorg.libXrender
    #     xorg.libXtst
    #     xorg.libxcb
    #     xorg.libxkbfile
    #     xorg.libxshmfence
    #   ];
    # };
    
    steam = lib.mkMerge [{
      enable = true;
      # on wayland, translate x11 input events to uinput events for steam input
      extest.enable = true;
      #
      localNetworkGameTransfers.openFirewall = true;
      protontricks.enable = true;
      remotePlay.openFirewall = true;
    }];

    xwayland.enable = true;
  }];

  qt = lib.mkMerge [{
    enable = true;
    style = "gtk2";
    platformTheme = "gtk2";
  }];

  gtk.iconCache.enable = true;
  
  xdg = lib.mkMerge [{
    portal = {
      config = {
        common = {
          default = [
            "xapp"
            # "kde"
            # "gtk"
          ];
        };
      };
      enable = true;
      extraPortals = with pkgs; [
        # xdg-desktop-portal-xapp
        xdg-desktop-portal-kde
        xdg-desktop-portal-gtk
      ];
    };
  }];
}
