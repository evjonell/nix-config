# This file defines overlays
{inputs, ...}: {
  # This one brings our custom packages from the 'pkgs' directory
  # additions = final: _prev: import ../pkgs final.pkgs;

  # This one contains whatever you want to overlay
  # You can change versions, add patches, set compilation flags, anything really.
  # https://nixos.wiki/wiki/Overlays
  # modifications = final: prev: {
  # };

  # When applied, the unstable nixpkgs set (declared in the flake inputs) will
  # be accessible through 'pkgs.unstable'
  unstable-packages = final: prev: {
    unstable = import inputs.nixos-unstable {
      system = prev.hostPlatform.system;
      config.allowUnfree = true;
    };
  };

  opt-unstable = final: prev: {
    optUnstable = import inputs.nixos-unstable {
      localSystem = final.system;
      hostPlatform = {
        system = "x86_64-linux";
        gcc.tune = "znver3";
        gcc.arch = "znver3";
      };
      config = {
        allowBroken = false;
        # Allow unfree packages
        allowUnfree = true;
        rocmSupport = true;
      };
    };
    myLqx = final.optUnstable.linuxPackagesFor ( final.optUnstable.linux_lqx.override {
      argsOverride = {
        structuredExtraConfig = with final.optUnstable.lib.kernel; {
          ZSWAP_COMPRESSOR_DEFAULT_LZ4 = no;
          ZSWAP_COMPRESSOR_DEFAULT_ZSTD = yes;
        };
      };
    });
  };

  # moz-packages = final: prev: {
  #   mozPkgs = (import inputs.nixpkgs {
  #     system = final.system;
  #     config = final.config;
  #     overlays = [
  #       inputs.mozilla-packages.overlay
  #     ];
  #   }).latest;
  # };

  # emacs-packages = final: prev: {
  #   emacsPkgs = (import inputs.nixpkgs {
  #     system = final.system;
  #     config = final.config;
  #     overlays = [
  #       inputs.emacs-overlay.overlay
  #     ];
  #   });
  # };

  # all-overlays = [
  #   inputs.emacs-overlay.overlay
  #   inputs.mozilla-packages.overlay
  #   unstable-packages
  # ];
}
