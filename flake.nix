{
  description = "Nixos system config flake";

  nixConfig = {
    extra-substituters = [
      # "https://nix-gaming.cachix.org"
      "https://nix-community.cachix.org"
    ];
    extra-trusted-public-keys = [
      # "nix-gaming.cachix.org-1:nbjlureqMbRAxR1gJ/f3hxemL9svXaZF/Ees8vCUUs4="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };
  
  inputs = {
    nixpkgs = {
      url = "github:nixos/nixpkgs/nixos-24.11";
    };
    nixos-unstable = {
      url = "github:nixos/nixpkgs/nixos-unstable";
      # url = "https://github.com/NixOS/nixpkgs/archive/refs/heads/nixos-unstable.zip";
    };
    # home-manager = {
    #   url = "github:nix-community/home-manager/release-24.11";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
    # nix-gaming = {
    #   url = "github:fufexan/nix-gaming/master";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
    nixos-hardware = {
      url = "github:NixOS/nixos-hardware/master";
    };
    mozilla-packages = {
      # url = "tarball+https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz";
      url = "github:mozilla/nixpkgs-mozilla/master";
    };
    emacs-overlay = {
      # url = "tarball+https://github.com/nix-community/emacs-overlay/archive/master.tar.gz";
      url = "github:nix-community/emacs-overlay/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  
  outputs = {
    nixpkgs,
    # home-manager,
    nixos-hardware,
    ...
  } @inputs:
# let
#   inherit (self) outputs;
#   lib = nixpkgs.lib; # // home-manager.lib;
#   systems = [
#     "x86_64-linux"
#     "aarch64-darwin"
#   ];
#   forAllSystems = lib.genAttrs systems;
#   pkgsFor = overlays: forAllSystems (
#     system:
#         import nixpkgs {
#           inherit system;
#           # Configure your nixpkgs instance
#           config = {
#             # Allow unfree packages
#             allowBroken = false;
#             allowUnfree = true;
#             rocmSupport = true;
#           };
#           inherit overlays;
#         }
#   );
# in
  rec {
    overlays = import ./overlays { inherit inputs; };
      
    nixosConfigurations = {
      mjolnir = nixpkgs.lib.nixosSystem rec {
        system = "x86_64-linux";
        specialArgs = {inherit inputs overlays;};
        modules = [
          ./mjolnir/configuration.nix
          # home-manager.nixosModules.home-manager {
          #   home-manager.useGlobalPkgs = true;
          #   home-manager.useUserPackages = true;
          #   home-manager.users.erika = import ./mjolnir/home.nix;
          #   home-manager.extraSpecialArgs = {inherit inputs outputs;};
          # }
        ];
      };

      aesir = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = {inherit inputs overlays;};
        modules = [
          nixos-hardware.nixosModules.system76
          ./aesir/configuration.nix
        ];
      };
    };
  };
}
