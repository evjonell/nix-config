# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ inputs, config, lib, pkgs, ... }:

{
  nixpkgs.config = {
    # Allow unfree packages
    allowUnfree = true;
    # allow manual unstable package overrides
    # packageOverrides = pkgs: {
    #   unstablePkgs = import (builtins.fetchTarball "channel:nixos-unstable") {
    #     config = config.nixpkgs.config;
    #   };
    # };
  };
  
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../shared-packages.nix
    ];
  
  # set various nix environment options
  nix = {
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
      trusted-users = [
        "root"
        "@wheel"
      ];
    };
    optimise = {
      automatic = true;
    };
  };


  
  # Kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;
  
  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;



  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  
  powerManagement = {
    enable = true;
  #   cpuFreqGovernor = "schedutil";
  };
  
  # Enable the X11 windowing system.
  services = {
    # acpid.enable = true;
    auto-cpufreq = {
      enable = true;
      settings = {
        battery = {
          governor = "powersave";
          turbo = "never";
        };
        charger = {
          governor = "performance";
          turbo = "auto";
        };
      };
    };

    dbus.enable = true;

    desktopManager = {
      plasma6.enable = true;
    };

    displayManager = {
      sddm = {
        enable = true;
        wayland.enable = true;
      };
    };

    flatpak.enable = true;

    libinput = {
      enable = true;

      # disabling mouse acceleration
      mouse = {
        accelProfile = "adaptive";
      };

      # disabling touchpad acceleration
      touchpad = {
        accelProfile = "adaptive";
      };
    };

    # Enable the OpenSSH daemon.
    # services.openssh.enable = true;
    
    # Enable sound with pipewire.
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      # If you want to use JACK applications, uncomment this
      #jack.enable = true;

      # use the example session manager (no others are packaged yet so this is enabled by default,
      # no need to redefine it in your config for now)
      #media-session.enable = true;
    };
    
    power-profiles-daemon.enable = false;

    # Enable CUPS to print documents.
    printing.enable = true;

    # tlp = {
    #   enable = true;
    #   settings = {
    #     CPU_SCALING_GOVERNOR_ON_AC = "performance";
    #     CPU_SCALING_GOVERNOR_ON_BAT = "powersave";

    #     CPU_ENERGY_PERF_POLICY_ON_BAT = "powersave";
    #     CPU_ENERGY_PERF_POLICY_ON_AC = "performance";

    #     CPU_MIN_PERF_ON_AC = 0;
    #     CPU_MAX_PERF_ON_AC = 100;
    #     CPU_MIN_PERF_ON_BAT = 0;
    #     CPU_MAX_PERF_ON_BAT = 20;

    #     #Optional helps save long term battery health
    #     START_CHARGE_THRESH_BAT0 = 40; # 40 and bellow it starts to charge
    #     STOP_CHARGE_THRESH_BAT0 = 80; # 80 and above it stops charging
    #   };
    # };
    
    xserver = {
      enable = true;
      xkb = {
        layout = "us";
        variant = "";
      };
      videoDrivers = [ "amdgpu" ];
      deviceSection = ''
        Option "DRI" "3"
      '';
      # Enable touchpad support (enabled default in most desktopManager).
      # services.xserver.libinput.enable = true;
      # libinput = {
      #   enable = true;

      #   # disabling mouse acceleration
      #   mouse = {
      #     accelProfile = "adaptive";
      #   };

      #   # disabling touchpad acceleration
      #   touchpad = {
      #     accelProfile = "adaptive";
      #   };
      # };
      # desktopManager = {
      #   plasma5.enable = true;
      #   gnome.enable = true;
      # };
      # displayManager = {
      #   sddm = {
      #     enable = true;
      #     wayland.enable = true;
      #   };
      #   gdm = {
      #     enable = true;
      #   };
      # };
    };
  };
    
  # qt = {
  #   enable = true;
  #   platformTheme = "gnome";
  #   style = "adwaita-dark";
  # };

  # Enable sound with pipewire.
  sound.enable = true;
  hardware = {
    # set to false because we use pipewire
    pulseaudio.enable = false;
    bluetooth = {
      enable = true;
      powerOnBoot = true;
    };
    opengl = {
      enable = true;
      extraPackages = with pkgs; [
        libva1
        libvdpau-va-gl
        mesa
      ];
      ## radv: an open-source Vulkan driver from freedesktop
      driSupport = true;
      driSupport32Bit = true;
    };
  };

  
  security = {
    polkit.enable = true;
    rtkit.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    groups = {
      nointernet = {
        name = "nointernet";
      };
      gamemode = {
        name = "gamemode";
      };
    };
    users = {
      erika = {
        isNormalUser = true;
        description = "Erika Jonell";
        extraGroups = [ "networkmanager" "wheel" "gamemode" ];
        # packages = with pkgs; [
          # kate
          # mesa-demos
          # protonup-qt
          # (import "/etc/nixos/dev-packages.nix" pkgs)
        # ];
        # shell = pkgs.fish;
      };
    };
  };


  
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment = {
    sessionVariables = {
      LIBVA_DRIVER_NAME = "radeonsi";
      AMD_VULKAN_ICD = "RADV";
    };
  };



    # networking.hostName = "aesir"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  # networking.networkmanager.enable = true;
  
  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # networking.firewall.enable = true;
  networking = {
    # Define your hostname.
    hostName = "aesir"; 
    # wireless.enable = true;  # Enables wireless support via wpa_supplicant.
    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  
    # Enable networking
    networkmanager.enable = true;
    # Open ports in the firewall.
    # networking.firewall.allowedTCPPorts = [ ... ];
    # networking.firewall.allowedUDPPorts = [ ... ];
    firewall = {
      enable = true;
      extraCommands = ''
        iptables -I OUTPUT 1 -m owner --gid-owner ${config.users.groups.nointernet.name} -j DROP
        ip6tables -I OUTPUT 1 -m owner --gid-owner ${config.users.groups.nointernet.name} -j DROP
      '';
    };
    useDHCP = lib.mkDefault true;
  };
  
  
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
