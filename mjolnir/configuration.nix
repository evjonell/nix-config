# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, inputs, options, overlays, pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      inputs.nixos-hardware.nixosModules.common-hidpi
      inputs.nixos-hardware.nixosModules.common-cpu-amd
      inputs.nixos-hardware.nixosModules.common-cpu-amd-pstate
      inputs.nixos-hardware.nixosModules.common-cpu-amd-zenpower
      inputs.nixos-hardware.nixosModules.common-gpu-amd
      inputs.nixos-hardware.nixosModules.common-pc-ssd
      ../shared-packages.nix
    ];

  # what we're going to build towards (if/when we build)
  nixpkgs = {
    overlays = [
      overlays.unstable-packages
      overlays.opt-unstable
      # inputs.emacs-overlay.overlay
      # inputs.mozilla-packages.overlay
    ];
    config = {
      allowBroken = false;
      allowUnfree = true;
      enableParallelBuildingByDefault = true;
      rocmSupport = true;
    };
  };

  
  nix = let
    flakeInputs = lib.filterAttrs (_: lib.isType "flake") inputs;
  in {
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
      cores = 16;
      max-jobs = 32;
      trusted-users = [
        "root"
        "@wheel"
      ];
      substituters = [
        "https://nix-community.cachix.org"
      ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];

      #
      # START OF Misterio77 STUFF
      # from https://github.com/Misterio77/nix-starter-configs/blob/main/standard/nixos/configuration.nix
      #
      ## Opinionated: disable global registry
      flake-registry = "";
      ## Workaround for https://github.com/NixOS/nix/issues/9574
      nix-path = config.nix.nixPath;
    };
    ## Opinionated: disable channels
    channel.enable = false;

    ## Opinionated: make flake registry and nix path match flake inputs
    registry = lib.mapAttrs (_: flake: {inherit flake;}) flakeInputs;
    nixPath = lib.mapAttrsToList (n: _: "${n}=flake:${n}") flakeInputs;
    #
    # END OF Misterio77 STUFF
    #
    optimise = {
      automatic = true;
    };
  };
  
  # additional boot settings
  boot = {
    # Kernel
    kernelModules = lib.mkMerge [[ "amdgpu" "tcp_bbr" ]];
    # kernelPackages = pkgs.unstable.linuxPackages_latest;
    # kernelPackages = pkgs.unstable.linuxPackages_lqx;
    kernelPackages = pkgs.myLqx;
    kernelParams = lib.mkMerge [[
      "amdgpu.dpm=1"
      "amdgpu.ppfeaturemask=0xfff7ffff"
    ]];
    # Bootloader.
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    kernel = {
      sysctl = {
        "net.core.default_qdisc" = "cake";
        "net.core.netdev_max_backlog" = 16384;
        "net.core.optmem_max" = 65536;
        "net.core.rmem_default" = 1048576;
        "net.core.rmem_max" = 16777216;
        "net.core.wmem_default" = 1048576;
        "net.core.wmem_max" = 16777216;
        "net.ipv4.ip_forward" = 1; # ip forwarding
        "net.ipv4.ipfrag_high_threshold" = 5242880; # Reduce the chances of fragmentation. Adjusted for SSD.
        "net.ipv4.tcp_congestion_control" = "bbr";
        "net.ipv4.tcp_dsack" = 1;                        # Enable Delayed SACK
        "net.ipv4.tcp_fastopen" = 3;
        "net.ipv4.tcp_keepalive_time" = 60;
        "net.ipv4.tcp_keepalive_intvl" = 10;
        "net.ipv4.tcp_keepalive_probes" = 6;
        "net.ipv4.tcp_mtu_probing" = 1;
        "net.ipv4.tcp_rmem" = "4096 1048576 16777216";
        "net.ipv4.tcp_sack" = 1;                        # Enable Selective Acknowledgment (SACK)
        "net.ipv4.tcp_slow_start_after_idle" = 0;       # Disable slow start after idle
        "net.ipv4.tcp_window_scaling" = 1;              # Enable TCP window scaling
        "net.ipv4.tcp_wmem" = "4096 16384 16777216";
        "net.ipv4.udp_rmem_min" = 8192;
        "net.ipv4.udp_wmem_min" = 8192;
        # virtual memory stuff
        "vm.dirty_background_bytes" = 134217728;    # 128 MB
        "vm.dirty_bytes" = 402653184;               # 384 MB
        "vm.min_free_kbytes" = 65536;               # Minimum free memory for safety (in KB), helping prevent memory exhaustion situations. Adjusted for 32GB RAM.
        "vm.swappiness" = 5;                        # Adjust how aggressively the kernel swaps data from RAM to disk. Lower values prioritize keeping data in RAM. Adjusted for 32GB RAM.
        "vm.vfs_cache_pressure" = 90;              # Adjust vfs_cache_pressure (0-1000) to manage memory used for caching filesystem objects. Adjusted for 32GB RAM.
      };
    };
  };

  # Set your time zone.
  time = {
    hardwareClockInLocalTime = true;
    timeZone = "America/New_York";
  };

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "en_US.UTF-8";
      LC_IDENTIFICATION = "en_US.UTF-8";
      LC_MEASUREMENT = "en_US.UTF-8";
      LC_MONETARY = "en_US.UTF-8";
      LC_NAME = "en_US.UTF-8";
      LC_NUMERIC = "en_US.UTF-8";
      LC_PAPER = "en_US.UTF-8";
      LC_TELEPHONE = "en_US.UTF-8";
      LC_TIME = "en_US.UTF-8";
    };
  };

  # specific programs related to mjolnir's config
  programs = lib.mkMerge [{
    sway = {
      enable = true;
    };
    thunar = {
      enable = true;
      plugins = with pkgs.xfce; [
        # enable thunar to act on archives
        thunar-archive-plugin
        thunar-volman
      ];
    };
  }];

  # List services that you want to enable:
  services = lib.mkMerge [{
    # network printing discovery
    avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
    };

    blueman.enable = true;
    
    colord.enable = true;
    
    dbus.enable = true;

    # desktopManager = {
    #   plasma6.enable = true;
    # };
    
    displayManager = {
      enable = true;
      defaultSession = "none+i3";
      # defaultSession = "sway";
      # defaultSession = "xfce";
      # defaultSession = "plasma";
      # sddm = {
      #   enable = true;
      #   wayland.enable = true;
      # };
      execCmd = lib.mkDefault "${pkgs.lightdm}/sbin/lightdm";
    };

    flatpak.enable = true;

    fwupd.enable = true;

    gvfs.enable = true;
    
    libinput = {
      enable = true;
      mouse = {
        accelProfile = "flat";
      };
    };

    # openssh = {

    # };
    
    picom = {
      enable = true;
      vSync = false;
    };
    
    # Enable sound with pipewire.
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
    
    # Enable CUPS to print documents.
    printing.enable = true;

    sunshine = {
      enable = true;
      autoStart = false;
      # capSysAdmin = true; # only needed for Wayland -- omit this when using with Xorg
      openFirewall = true;
    };
    
    udev = {
      enable = true;
      extraRules = ''
        # HDD
        ACTION=="add|change", KERNEL=="sd[a-z]*", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"

        # SSD
        ACTION=="add|change", KERNEL=="sd[a-z]*|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="bfq"

        # NVMe SSD
        ACTION=="add|change", KERNEL=="nvme[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="bfq"
      '';
    };

    # Enable the X11 windowing system.
    xserver = lib.mkMerge [{
      enable = true;
      exportConfiguration = true;
      extraConfig = ''
        Section "OutputClass"
            Identifier "AMD"
            MatchDriver "amdgpu"
            Driver "amdgpu"
            Option "DRI" "3"
            Option "VariableRefresh" "false"
        EndSection
        Section "Screen"
            Identifier     "Screen"
            DefaultDepth    30
            SubSection      "Display"
                Depth   30
            EndSubSection
        EndSection
      ''; 
      desktopManager = {
        xterm.enable = false;
        
        # xfce = {
        #   enable = true;
        # };
      };
      # displayManager = {
      #   # defaultSession = "xfce";
      #   lightdm = {
      #     enable = true;
      #   };
      # };
      videoDrivers = [ "amdgpu" ];
      windowManager = lib.mkMerge [{
        i3 = {
          enable = true;
          # package = pkgs.i3-wm;
          extraPackages = with pkgs; [
            dmenu # application launcher most people use
            dunst # notification manager
            # i3status
            i3status-rust # gives you the default i3 status bar
            i3lock # default i3 screen locker
            # i3blocks # if you are planning on using i3blocks over i3status
            rofi # app launcher
          ];
        };
      }];
      xkb = {
        layout = "us";
        variant = "";
      };
    }];
  }];

  hardware = lib.mkMerge [{
    # cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
    # enable all firmware regardless of license
    enableAllFirmware = true;
    # set to false because we use pipewire
    pulseaudio.enable = false;
    bluetooth = {
      enable = true;
      # ensure that even if the default changes, we always try to power it on
      powerOnBoot = true;
      # better settings
      settings = {
        General = {
          JustWorksRepairing = "always";
          FastConnectable = true;
          Class = "0x7c0104";
        };
        GATT = {
          AutoEnable = true;
          ReconnectIntervals = "1,1,2,3,5,8,13,21,34,55";
        };
      };
    };
    graphics = lib.mkMerge [{
      enable = true;
      enable32Bit = true;
      extraPackages = with pkgs; [
        # amdvlk
        # libva
        libvdpau-va-gl
        mesa.drivers
        mesa.opencl
      ];
      extraPackages32 = with pkgs; [
        # libva
        driversi686Linux.libvdpau-va-gl
        driversi686Linux.mesa.drivers
        driversi686Linux.mesa.opencl
      ];
    }];
    # support for xbox controllers
    xone.enable = true;
    xpadneo.enable = true;
  }];

  
  security = lib.mkMerge [{
    # allow SMT
    allowSimultaneousMultithreading = true;
    polkit.enable = true;
    rtkit.enable = true;
    pam = {
      mount = {
        enable = true;
        createMountPoints = true;
      };
    };
  }];


  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    users = {
      erika = {
        isNormalUser = true;
        description = "Erika Jonell";
        extraGroups = [
          "audio"
          "gamemode"
          "networkmanager"
          "nix-users"
          "wheel"
        ];
      };
    };
  };


  # set env vars
  environment = lib.mkMerge [{
    # for i3
    pathsToLink = lib.mkMerge[ [ "/libexec" ]];
    # mjolnir specific packages
    systemPackages = lib.mkMerge [[
      #
      # SYSTEM
      #
      ## add cpupower for kernel
      config.boot.kernelPackages.cpupower
      #
      # for i3
      #
      pkgs.lxappearance
      pkgs.lxsession
      # pkgs.xfce.thunar
      #
      # XFCE
      #
      ## for thunar
      # pkgs.xfce.thunar-archive-plugin
      # pkgs.xfce.thunar-volman
      ## add whisker menu
      # pkgs.xfce.xfce4-whiskermenu-plugin
      #
      # KDE
      #
      ## kwallet
      pkgs.kdePackages.kwallet
      pkgs.kdePackages.kwallet-pam
      pkgs.kdePackages.kwalletmanager
      pkgs.kwalletcli
      ## ssh helper
      pkgs.kdePackages.ksshaskpass
      ## KDE PLASMA 6
      pkgs.kdePackages.kate
      pkgs.kdePackages.discover
      # kdePackages.sddm-kcm
      #
      # SWAY
      #
      pkgs.bemenu
      pkgs.gammastep
      pkgs.waybar
      pkgs.wl-clipboard
    ]];
    sessionVariables = lib.mkMerge [ rec {
      #
      # gpu
      #
      AMD_VULKAN_ICD = "RADV";
      #
      # libva
      #
      LIBVA_DRIVER_NAME = "radeonsi";
      LIBVA_DRIVERS_PATH = "${pkgs.mesa.driverLink}/lib/dri";
      LIBVA_DRM_DEVICE = "/dev/dri/renderD128";
      #
      # vulkan
      #
      # VK_DRIVER_FILES = "${pkgs.mesa_i686.driverLink}/share/vulkan/icd.d:radeon_icd.i686.json:${pkgs.mesa.driverLink}/share/vulkan/icd.d/radeon_icd.x86_64.json";
      #
      # xdg
      #
      XDG_CACHE_HOME  = "$HOME/.cache";
      XDG_CONFIG_HOME = "$HOME/.config";
      XDG_DATA_HOME   = "$HOME/.local/share";
      XDG_STATE_HOME  = "$HOME/.local/state";
      ## Not officially in the specification
      XDG_BIN_HOME    = "$HOME/.local/bin";
      PATH = [ 
        "${XDG_BIN_HOME}"
      ];
    }];
  }];

  # Enable networking
  networking = lib.mkMerge [{
    # ipv6 causes too many problems
    enableIPv6 = false;

    # use nftables instead of iptables (better)
    nftables = {
      enable = true;
    };
    
    #firewall settings
    firewall = lib.mkMerge [{
      enable = true;
      # Open ports in the firewall.
      allowedTCPPortRanges = [
        # ffxiv
        {
          from = 54992;
          to = 54994;
        }
        {
          from = 55006;
          to = 55007;

        }
        {
          from = 55021;
          to= 55040;
        }
      ];
      allowedTCPPorts = [
        #battle.net
        1119
        #wow
        3724
        6012
      ];
      allowedUDPPortRanges = [
        # ffxiv
        {
          from = 54992;
          to = 54994;
        }
        {
          from = 55006;
          to = 55007;

        }
        {
          from = 55021;
          to = 55040;
        }
      ];
      allowedUDPPorts = [
        #battle.net
        1119
        #wow
        3724
        6012
      ];
    }];
    
    # Define your hostname.
    hostName = "mjolnir";

    # for port forwarding
    nat = {
      enable = true;
      enableIPv6 = false;
    };
    
    # enable network manager
    networkmanager = {
      enable = true;
      appendNameservers = [
        # nextdns (customized)
        "45.90.28.58" "45.90.30.58"
        # cloudflare (speed)
        "1.1.1.1" "1.0.0.1"
        # quad9 (security)
        "9.9.9.9" "149.112.112.112"
        # google (broad fallback)
        "8.8.8.8" "8.8.4.4"
      ];
    };

    timeServers = options.networking.timeServers.default;
    # ++ [
    #   "0.nixos.pool.ntp.org"
    #   "1.nixos.pool.ntp.org"
    #   "2.nixos.pool.ntp.org"
    #   "3.nixos.pool.ntp.org"
    #   "time.google.com"
    #   "time2.google.com"
    #   "time3.google.com"
    #   "time4.google.com"
    # ];
  }];

  # systemd stuff
  systemd = lib.mkMerge [{
    tmpfiles = {
      rules = lib.mkMerge [[
        #  Path                  Mode UID  GID  Age Argument
        "w /proc/sys/vm/compaction_proactiveness - - - - 0"
        "w /proc/sys/vm/watermark_boost_factor - - - - 1"
        "w /proc/sys/vm/min_free_kbytes - - - - 1048576"
        "w /proc/sys/vm/watermark_scale_factor - - - - 500"
        "w /proc/sys/vm/swappiness - - - - 10"
        "w /sys/kernel/mm/lru_gen/enabled - - - - 5"
        "w /proc/sys/vm/zone_reclaim_mode - - - - 0"
        "w /sys/kernel/mm/transparent_hugepage/enabled - - - - madvise"
        "w /sys/kernel/mm/transparent_hugepage/shmem_enabled - - - - advise"
        "w /sys/kernel/mm/transparent_hugepage/defrag - - - - never"
        "w /proc/sys/vm/page_lock_unfairness - - - - 1"
        "w /proc/sys/kernel/sched_child_runs_first - - - - 0"
        "w /proc/sys/kernel/sched_autogroup_enabled - - - - 1"
        "w /proc/sys/kernel/sched_cfs_bandwidth_slice_us - - - - 3000"
        "w /sys/kernel/debug/sched/base_slice_ns  - - - - 3000000"
        "w /sys/kernel/debug/sched/migration_cost_ns - - - - 500000"
        "w /sys/kernel/debug/sched/nr_migrate - - - - 8"
      ]];
    };
    #
    # my services
    #
    ## setup an auth agent for polkit
    user.services.lxpolkit = {
      description = "lxsession polkit auth agent";
      wantedBy = [ "graphical-session.target" ];
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.lxsession}/bin/lxpolkit";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };
  }];
  
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?
}
