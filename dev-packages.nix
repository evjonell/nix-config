{ pkgs, ... }:

  let
    rustPkgs = with pkgs; [
      bacon
      cargo-watch
      cargo-info
      emacs-lsp-booster
      rust-analyzer
      rustfmt
      taplo
    ];
    
    nodePkgs = with pkgs.nodePackages; [
      bash-language-server
      eslint
      pnpm
      prettier
      stylelint
      typescript
      typescript-language-server
      vscode-langservers-extracted
      yaml-language-server
      yarn
    ];

    luaPkgs = with pkgs; [
      lua-language-server
    ];
    
    dotnetSdkPkgs = (with pkgs.dotnetCorePackages; combinePackages [
      sdk_6_0
      sdk_7_0
      sdk_8_0
    ]);

    dotnetSdkDeps = [
      dotnetSdkPkgs
    ];
    
    dotnetRuntimePkgs = (with pkgs.dotnetCorePackages; combinePackages [
      runtime_6_0
      runtime_7_0
      runtime_8_0
    ]);
    
    dotnetRuntimeDeps = [dotnetRuntimePkgs] ++ ( with pkgs; [
      roslyn
      msbuild
      mono
      omnisharp-roslyn
    ]);
    
    nativeBuildInputs = with pkgs; [
      pkg-config
      clang
      llvm
      llvmPackages.bintools
    ];

    buildInputs = dotnetSdkDeps ++ (with pkgs; [
      markdown-link-check
    ]);

    userDevPackages = luaPkgs ++ nodePkgs ++ rustPkgs ++ (with pkgs; [
      bun
      cargo
      deno
      lua
      marksman
      nodejs_20
      rustc
    ]);
    in {
      environment = {
        systemPackages =
          nativeBuildInputs
          ++ buildInputs
          ++ userDevPackages
          ++ dotnetSdkDeps
          ++ dotnetRuntimeDeps
          ++ (with pkgs; [
            emacs-lsp-booster
          ]);
        sessionVariables = {
          NIX_LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath buildInputs;
          DOTNET_ROOT = "${dotnetSdkPkgs}";
        };
      };
    }
